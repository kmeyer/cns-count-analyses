#!/usr/bin/env python3
import csv
import xlrd

wb = xlrd.open_workbook("prabhakar2006accelerated-supplement.xls")

elems = {"hacns": "Table S1A",
         "cacns": "Table S1B",
         "macns": "Table S1C"}

for elem, sname in elems.items():
    sh = wb.sheet_by_name(sname)
    with open(elem + "-info.csv", "w") as cf:
        wr = csv.writer(cf, quoting=csv.QUOTE_ALL)
        wr.writerows([sh.row_values(i) for i in range(sh.nrows)])
