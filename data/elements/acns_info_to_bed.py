#!/usr/bin/env python3
import csv

elems = ["hacns", "cacns", "macns"]

for elem in elems:
    with open(elem + "-info.csv") as ifh:
        # Skip header.
        next(ifh)

        with open(elem + "-coordinates-hg17.bed", "w") as ofh:
            for fields in csv.reader(ifh):
                name, chrm, start, stop = fields[:4]
                name = elem + "." + str(int(float(name)))
                # Remove ".0".
                start, stop = int(float(start)), int(float(stop))
                ofh.write("\t".join([chrm, str(start), str(stop), name]) + "\n")
