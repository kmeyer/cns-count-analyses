#!/usr/bin/env python3
"""Subset and prepare kang2011spatio data.

- Add binary pass/fail vector for DABG.

- Subset to gencode v19 protein-coding genes.

- If gene is assigned to more than one transcript cluster ID, take the
  one with the most direct probes.  Breaks ties with coverge score.
"""

import numpy as np
import pandas as pd

from pdutil import filter_repeats

ge = pd.read_csv("GSE25219-GPL5175-expression.txt", sep="\t", index_col=0)
sampleinfo = pd.read_csv("kang2011spatio-sampleinfo.csv", index_col=0)
geneinfo = pd.read_csv("gpl5175-geneinfo-gencode.csv", index_col=0)

## Limit to gencode genes.
geg = ge[ge.index.isin(geneinfo.index)]
geg.index.name = "affyid"
geneinfo = geneinfo[geneinfo.index.isin(geg.index)]

geneinfo = filter_repeats(geneinfo, "gc_symbol", ["dir_probes", "coverage"])
geg = geg[geg.index.isin(geneinfo.index)]

assert (geneinfo.index == geg.index).all()

dabg = pd.read_csv("GSE25219_DABG_pvalue.csv.gz", index_col=0)
# dinfo = dabg.columns.str.split(".")
# assert np.all(dinfo.str[0].str.replace("X", "").astype(int) == sampleinfo["indiv"])
# assert np.all(sampleinfo[dinfo.str[1] != sampleinfo["region"]]["region"].unique() == "M1C&S1C")
dabg.columns = geg.columns
dabg = dabg.reindex(index=geg.index)

## Exclude combined "M1C&S1C" region in period 5 and 6.
region_bm = sampleinfo["stage"].isin([5, 6]) & (sampleinfo["region"] == "M1C&S1C")
geg = geg.loc[:, ~region_bm]

sampleinfo = sampleinfo.reindex(index=geg.columns)
dabg = dabg.reindex(columns=geg.columns)

dabg_mean = dabg.groupby([sampleinfo["stage"],
                          sampleinfo["region"]], axis=1).mean()
pass_dabg = (dabg_mean <= 0.01).any(axis=1)

## Add integer labels for some sample features.

regions = ["FC", "M1C&S1C", "PC", "TC", "OC",
           "VF", "LGE", "MGE", "CGE", "DIE", "DTH", "URL",
           "OFC", "DFC", "VFC", "MFC", "M1C", "S1C", "IPC", "A1C",
           "STC", "ITC", "V1C", "HIP", "AMY", "STR", "MD", "CBC"]
reg_ncx = {"OFC": 0, "DFC": 0, "VFC": 0, "MFC": 0, "M1C": 0, "S1C": 0,
           "IPC": 0, "A1C": 0, "STC": 0, "ITC": 0, "V1C": 0,
           "FC": 0, "M1C&S1C": 0, "PC": 0, "TC": 0, "OC": 0,
           "HIP": 1, "AMY": 2, "STR": 3, "MD" : 4, "CBC": 5,
           "VF": 3, "LGE": 3, "MGE": 3, "CGE": 3,
           "DIE": 4, "DTH": 4,
           "URL": 5}

sampleinfo["db"] = sampleinfo["region"].map(regions.index)
sampleinfo["dr"] = sampleinfo["region"].map(reg_ncx)
sampleinfo["ds"] = sampleinfo["stage"] - sampleinfo["stage"].min()
sampleinfo["di"] = np.unique(sampleinfo["indiv"].values,
                             return_inverse=True)[1]

geg.to_csv("kang2011spatio-p1-ge-subset.csv")
dabg.to_csv("kang2011spatio-p1-dabg-subset.csv")
pass_dabg.to_csv("kang2011spatio-p1-pass_dabg-subset.csv")
sampleinfo.to_csv("kang2011spatio-p1-sampleinfo-subset.csv")
geneinfo.to_csv("kang2011spatio-p1-geneinfo-subset.csv")
