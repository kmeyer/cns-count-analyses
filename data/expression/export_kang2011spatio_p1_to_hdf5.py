#!/usr/bin/env python3
"""Export kang2011spatio-p1.h5 values to standard HDF5 format.
"""

import pandas as pd
import h5py

datafile = "kang2011spatio-p1.h5"
with pd.get_store(datafile, mode="r") as store:
    ge = store["ge"]
    dabg = store["dabg"]
    pass_dabg = store["pass_dabg"]
    si = store["sampleinfo"]

with h5py.File("kang2011spatio-p1.hdf5", mode="w") as hf:
    hf.create_dataset("values", data=ge.values, compression="gzip")
    hf.create_dataset("pass_dabg", data=pass_dabg.values, compression="gzip")
    hf.create_dataset("di", data=si["di"].values, compression="gzip")
    hf.create_dataset("ds", data=si["ds"].values, compression="gzip")
    hf.create_dataset("dr", data=si["dr"].values, compression="gzip")
    hf.create_dataset("db", data=si["db"].values, compression="gzip")
    hf.create_dataset("rin", data=si["rin"].values, compression="gzip")
    hf.create_dataset("pmi", data=si["pmi"], compression="gzip")
