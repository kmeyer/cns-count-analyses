#!/usr/bin/env python3

import pandas as pd

datafile = "gtex.h5"
with pd.get_store(datafile, mode="r") as store:
    gi = store["geneinfo"]

gi["gname"].to_csv("gtex-gene-names.txt", index=False)
