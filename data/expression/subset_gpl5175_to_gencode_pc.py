#!/usr/bin/env python3
"""Filter and subset gpl5175-geneinfo.csv.

* Filter out assignments to ENSEMBL id_type.

* Subset to ENSGs that match gencode protein coding genes.

* If an transcript cluster maps to more than one gene, take the gene
  with the highest number of direct probes.  If there is a tie here,
  discard the gene.
"""
import json

import numpy as np
import pandas as pd

gi = pd.read_csv("gpl5175-geneinfo.csv")

idless = gi[gi[["symbol", "ensg"]].isnull().all(1)]
assert (idless.id_type == "RefSeq").all()
# In [5]: idless[idless.id_type == "RefSeq"]["score"].max()
# Out[5]: 38

gi = gi[gi["id_type"] == "ENSEMBL"].copy()

gencode_file = "../gencode/gencode-v19-genename-ensg.json"
with open(gencode_file) as gf:
    symbol_to_ensgs = json.load(gf)
ensg_to_symbol = {v: k for k, vs in symbol_to_ensgs.items()
                  for v in vs}
gencode_ensgs = set(ensg_to_symbol.keys())

gi = gi[gi["ensg"].isin(gencode_ensgs)]
gi["gc_symbol"] = gi["ensg"].map(ensg_to_symbol)

gi.sort_values(["affyid", "dir_probes", "ensg"],
               ascending=[True, False, True], inplace=True)

assert gi.duplicated().sum() == 0

max_dir_probes = gi.groupby("affyid")["dir_probes"].transform(np.max)
gi_max = gi[gi["dir_probes"] == max_dir_probes]
uniq_max = gi_max.groupby("affyid")["gc_symbol"].nunique() == 1
gi_max = gi_max[gi_max.affyid.isin(uniq_max[uniq_max].index)]
gi_max = gi_max[~gi_max[["affyid", "gc_symbol"]].duplicated()]

assignments = gi.groupby("affyid")["gc_symbol"].unique().str.join("|")
gi_max["assignments"] = assignments.reindex(index=gi_max["affyid"]).values
gi_max.to_csv("gpl5175-geneinfo-gencode.csv", index=False)

tied = gi[~gi["affyid"].isin(gi_max["affyid"])]
tied.to_csv("gpl5175-geneinfo-gencode-tied.csv", index=False)
