#!/usr/bin/env python3
"""Take standardized median of NCX areas medians for each stage.
"""

import h5py
import pandas as pd

datafile = "kang2011spatio-p1.hdf5"
with h5py.File(datafile, mode="r") as hf:
    dat = pd.DataFrame(hf["values"][:])
    ds = hf["ds"][:]
    dr = hf["dr"][:]
    db = hf["db"][:]

ncx_bm = dr == 1
dat_ncx = dat.iloc[:, ncx_bm]
db_ncx = db[ncx_bm]

ds_ncx = ds[ncx_bm]

stage_meds = dat_ncx.groupby([ds_ncx, db_ncx], axis=1).median()
stage_meds.columns.names = ["stage", "region"]

meds = stage_meds.groupby(axis=1, level="stage").median()
meds_std = (meds - meds.values.mean()) / meds.values.std()

meds_std.to_csv("kang2011spatio-p1-ncx-std-median-per-stage.dat", sep=" ",
                header=None, index=None)
