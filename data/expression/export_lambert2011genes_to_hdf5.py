#!/usr/bin/env python3
"""Export johnson2009functional.h5 to standard HDF5 format.
"""

import pandas as pd
import h5py

datafile = "lambert2011genes.h5"
with pd.get_store(datafile, mode="r") as store:
    ge = store["ge"]
    si = store["sampleinfo"]

with h5py.File("lambert2011genes.hdf5", mode="w") as hf:
    hf.create_dataset("values", data=ge.values, compression="gzip")
    hf.create_dataset("di", data=si["di"].values, compression="gzip")
    hf.create_dataset("dr", data=si["dr"].values, compression="gzip")
