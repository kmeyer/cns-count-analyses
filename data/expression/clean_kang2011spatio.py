#!/usr/bin/env python3
"""Clean up sample information from series matrix file.
"""

import numpy as np
import pandas as pd

with open("GSE25219-GPL5175-expression.txt") as fh:
    header = next(fh)
    samples = [s[1:-1] for s in header.strip().split("\t")][1:]

### Clean up sample info.

sub_keys = {"Stage": "stage",
            "Sex": "sex",
            "rna integrity number": "rin",
            "brain code": "indiv",
            "hemisphere": "side",
            "postmortem interval": "pmi"}

sample_info = {}
with open("GSE25219-GPL5175-info.txt") as fh:
    for line in fh:
        fields = [f[1:-1] for f in line.strip().split("\t")[1:]]
        names, values = zip(*[f.split(": ") for f in fields])

        name = names[0]
        if name in sub_keys:
            name = sub_keys[name]
        sample_info[name] = values

sdf = pd.DataFrame(sample_info)
columns = ["indiv", "region", "side", "sex", "age",
           "stage", "pmi", "ph", "rin"]
sdf = sdf.reindex(columns=columns)
sdf.index = samples
sdf.index.name = "sample"

sdf.replace("NA", np.nan, inplace=True)
sdf["pmi"].replace("1~3", "3", inplace=True)
sdf["pmi"].replace(">13", "15", inplace=True)
sdf["indiv"] = sdf["indiv"].str.replace("HSB", "")

sdf = sdf.convert_objects(convert_numeric=True)

# 1 missing RIN value.  Fill with average for individual.

null_rin_idx, = sdf[sdf["rin"].isnull()].index
missing_indiv = sdf.loc[null_rin_idx, "indiv"]
sdf.loc[null_rin_idx, "rin"] = sdf[sdf["indiv"] == missing_indiv]["rin"].mean()

sdf.to_csv("kang2011spatio-sampleinfo.csv")
