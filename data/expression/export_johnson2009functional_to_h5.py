#!/usr/bin/env python3
"""Store in johnson2009functional in Pandas HDF5 file.
"""

import pandas as pd

si = pd.read_csv("johnson2009functional-sampleinfo-subset.csv",
                 index_col=0)
gi = pd.read_csv("johnson2009functional-geneinfo-subset.csv",
                 index_col=0)
ge = pd.read_csv("johnson2009functional-ge-subset.csv",
                 index_col=0)

## np.nans will cause issue with mixed type saving to HDF5.
si.side.fillna("", inplace=True)
gi["symbol"].fillna("", inplace=True)

h5name = "johnson2009functional.h5"
with pd.get_store(h5name, mode="w", complevel=4) as store:
    store["sampleinfo"] = si
    store["geneinfo"] = gi
    store["ge"] = ge
