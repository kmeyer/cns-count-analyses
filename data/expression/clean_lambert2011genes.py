#!/usr/bin/env python3

import numpy as np
import pandas as pd
import re

with open('GSE21858-expression.txt') as fh:
    header = next(fh)
    samples = [s[1:-1] for s in header.strip().split('\t')][1:]

### Clean up sample info.

## Lines: age, region, and side.
with open('GSE21858-info.txt') as fh:
    sinfo = {k: line.strip().split('\t')[1:]
             for k, line in zip(['region', 'side', 'age'], fh)}

sinfo['age'] = [int(x.split(' ')[1]) for x in sinfo['age']]

side_abbrvs = {'left': 'L', 'right': 'R'}
sinfo['side'] = [side_abbrvs[x[:-1].split(': ')[1]] for x in sinfo['side']]

region_abbrvs = {'prefrontal/orbito-frontal cortex': 'poc',
                 'parieto-temporal cortex': 'ptc'}
sinfo['region'] = [region_abbrvs[x[:-1].split(': ')[1]] for x in sinfo['region']]

si = pd.DataFrame(sinfo)
si.index = samples
si.index.name = 'sample'

# In [28]: si.groupby(['age', 'region', 'side']).size()
# Out[28]:
# age  region  side
# 17   poc     L       1
#              R       1
#      ptc     L       1
#              R       1
# 19   poc     L       1
#              R       1
#      ptc     L       1
#              R       1
# dtype: int64

# In [29]: si.groupby(['age', 'region']).size()
# Out[29]:
# age  region
# 17   poc       2
#      ptc       2
# 19   poc       2
#      ptc       2
# dtype: int64
si["di"] = np.unique(si["age"].values, return_inverse=True)[1]
si["dr"] = np.unique(si["region"].values, return_inverse=True)[1]

si.to_csv('lambert2011genes-sampleinfo.csv')
