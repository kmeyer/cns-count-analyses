#!/usr/bin/env python3

import pandas as pd

ge = pd.read_csv("gtex-counts-subset.csv", index_col=0)
si = pd.read_csv("gtex-sattr-subset.csv", index_col=0)
gi = pd.read_csv("gtex-genes-subset.csv")
tissues = pd.read_csv("gtex-tissues-subset.csv",
                      index_col=0, squeeze=True)

with pd.get_store("gtex.h5", mode="w", complevel=4) as store:
    store["ge"] = ge
    store["sampleinfo"] = si
    store["geneinfo"] = gi
    store["tissues"] = tissues
