#!/usr/bin/env python3
"""Take stanardized median of region (NCX as one) medians for each
stage.
"""

import h5py
import pandas as pd

datafile = "kang2011spatio-p1.hdf5"
with h5py.File(datafile, mode="r") as hf:
    dat = pd.DataFrame(hf["values"][:])
    ds = hf["ds"][:]
    dr = hf["dr"][:]

stage_meds = dat.groupby([ds, dr], axis=1).median()
stage_meds.columns.names = ["stage", "region"]

meds = stage_meds.groupby(axis=1, level="stage").median()

meds_std = (meds - meds.values.mean()) / meds.values.std()

meds_std.to_csv("kang2011spatio-p1-region-std-median-per-stage.dat",
                sep=" ", header=None, index=None)
