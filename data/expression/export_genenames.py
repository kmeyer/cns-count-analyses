#!/usr/bin/env python3

import pandas as pd

datafile = "kang2011spatio-p1.h5"
with pd.get_store(datafile, mode="r") as store:
    gi = store["geneinfo"]

gi["gc_symbol"].to_csv("kang2011spatio-gene-names.txt", index=False)
