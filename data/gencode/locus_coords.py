#!/usr/bin/env python3

from itertools import groupby


def locus_coords(spacings, end_values=None):
    """Define locus for coords based on `spacings`.

    `spacings` should be a list of lists, with each inner list
    representing the output of "bedtools spacing":

          [chromosome, start, end, id, spacing]

    The lists should be sorted by chromosome and start.  Spacing for
    the first element of a chromosome is ignored.

    Any coordinates that overlap are filtered out.

    If `end_values` is not None, it should be a dictionary, with
    chromosomes as keys, indicating the value to be used for the last
    coordinate of a chromosome.
    """
    if end_values is None:
        end_values = {}

    for chrm, coords in groupby(spacings, lambda x: x[0]):
        last_beg, last_end, last_ident = next(coords)[1:4]
        last_flank = last_beg
        skip_next = False
        beg = None
        ident = None

        for _, beg, end, ident, spacing in coords:
            flank = spacing // 2

            if spacing != 0 and not skip_next:
                yield chrm, last_beg - last_flank, last_end + flank, last_ident
            elif spacing == 0:
                skip_next = True
            elif skip_next:
                skip_next = False

            last_ident = ident
            last_flank = flank
            last_beg, last_end = beg, end

        if not skip_next:
            yield chrm, beg - flank, end_values.get(chrm, None), ident


def parse_spacing_line(line):
    chrm, start, end, ident, _, _, spacing = line.strip().split("\t")
    if spacing == ".":
        spacing = None
    else:
        spacing = int(spacing)
    return chrm, int(start), int(end), ident, spacing

if __name__ == '__main__':
    with open("hg19.chrom.sizes") as cf:
        chrm_lens = {}
        for line in cf:
            chrm, length = line.strip().split("\t")
            chrm_lens[chrm] = int(length)

    with open("gencode.v19.annotation-transcript-pc-longest.bed.spacing") as sf:
        spacings = (parse_spacing_line(line) for line in sf)
        for outline in locus_coords(spacings, chrm_lens):
            try:
                print("\t".join([str(i) for i in outline]))
            except BrokenPipeError:
                break
