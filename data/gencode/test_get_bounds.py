import get_bounds


def test_store_bounds():
    fields = [("_", 15, 20, "id1", "_"),
              ("_", 10, 30, "id1", "_"),
              ("_", 50, 60, "id2", "_"),
              ("_", 70, 80, "id3", "_"),
              ("_", 75, 85, "id3", "_"),]
    bounds = get_bounds.store_bounds(fields)

    assert bounds["id1"]["start"] == 10
    assert bounds["id1"]["stop"] == 30

    assert bounds["id2"]["start"] == 50
    assert bounds["id2"]["stop"] == 60

    assert bounds["id3"]["start"] == 70
    assert bounds["id3"]["stop"] == 85
