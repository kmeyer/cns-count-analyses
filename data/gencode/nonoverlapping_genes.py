#!/usr/bin/env python3

from itertools import groupby

genes = []
with open("gencode.v19.annotation-transcript-pc-longest.bed.spacing") as sf:
    for chrm, lines in groupby(sf, lambda x: x.split("\t")[0]):
        last_pop = False
        for line in lines:
            fields = line.strip().split("\t")
            ident, spacing = fields[3], fields[6]
            if spacing == "0":
                if not last_pop:
                    genes.pop()
                    last_pop = True
            else:
                genes.append(ident)
                last_pop = False

with open("nonoverlapping-genes.txt", "w") as ofh:
    ofh.write("\n".join(genes) + "\n")
