#!/usr/bin/env python3
"""Extract ENST from each GTF line.

Usage: get_ensts.py [GTF...]
"""

from docopt import docopt
import fileinput
import re

_ = docopt(__doc__)

enst_pat = re.compile(r'transcript_id "(ENST[R]{0,1}\d+)\.\d+";')

try:
    for line in fileinput.input():
        print(enst_pat.search(line).group(1))
except BrokenPipeError:
    pass
