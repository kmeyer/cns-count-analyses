#!/usr/bin/env python3
"""Create matrix of a metric for nearest genes to elements.

Usage: make_nearest_matrix.py CLIST GENES OUTFILE

Arguments:
  CLIST
      A file where line provides an element name (to be used as the
      column name in the matrix) and the file from which to read the
      values.  Each path listed in CLIST should be relative to the
      directory that CLIST is in.

  GENES
      File with newline-delimted list of genes that were considered
      when generating CLIST sets

  OUTFILE
      File to write matrix to (as CSV)
"""

import os
import sys

import numpy as np
import pandas as pd

try:
    _, cfile, gfile, outfile = sys.argv
except ValueError:
    sys.exit(__doc__)

with open(gfile) as fh:
    genes = [g.strip() for g in fh.readlines()]

cfile_dir = os.path.dirname(cfile)

with open(cfile) as fh:
    lines = (ln.strip() for ln in fh)
    el_files = (ln.split() for ln in lines if ln)
    el_files = [(e, os.path.join(cfile_dir, f)) for e, f in el_files]

elements = list(zip(*el_files))[0]

nearest = {}
for el, fname in el_files:
    nearest[el] = {}
    with open(fname) as fh:
        for gene_count in fh:
            gene, count = gene_count.strip().split()
            nearest[el][gene] = int(count)

ng = pd.DataFrame.from_dict(nearest)
ng = ng.reindex(columns=elements, index=genes)
ng.fillna(value=0, inplace=True)
ng = ng.astype(np.int)
ng.index.name = "gene"
ng.to_csv(outfile)
