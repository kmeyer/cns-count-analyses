#!/usr/bin/env python3
"""Keep elements that are at least MINDIST but not at or over MAXDIST.

USAGE: filter_nearest_within.py MINDIST MAXDIST
"""

import fileinput
import sys

try:
    min_dist, max_dist = int(sys.argv[1]), int(sys.argv[2])
except (IndexError, ValueError):
    sys.exit(__doc__)

for line in fileinput.input(files="-"):
    distance = int(line.split("\t")[10])
    if min_dist <= abs(distance) < max_dist:
        sys.stdout.write(line)
