#!/usr/bin/env python3

import json
import numpy as np
import pandas as pd

import rdump

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")

period_idx = 5

with pd.HDFStore("../data/expression/kang2011spatio-p1.h5") as store:
    gi = store["geneinfo"]

cols = ["ocns_intronic", "ocns_near", "ocns_far"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts-dist.csv")
ng = ng[cols]
ng = np.log1p(ng)

ragg = np.loadtxt("../data/nearest-gene/kang2011spatio-randagg.dat")
ragg = (ragg - np.mean(ragg)) / np.std(ragg)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values,
               ragg[:, None]])

data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data, "dp__regde_per6_dist_ragg.data.r")
