#!/usr/bin/env python3

import pandas as pd
from scipy.stats import norm

import mcmcstats
import stanio

dfiles = ["samples/dp__regde_per6_dist-all-chains.j4.csv",
          "samples/dp__regde_per6_dist_ragg-all-chains.j4.csv"]

for fname in dfiles:
    print("\n* {}".format(fname))
    samples = stanio.read_samples(fname, filter_svars=True)

    diffs = {"intronic_near": ["beta__2", "beta__3"],
             "intronic_far": ["beta__2", "beta__4"],
             "near_far": ["beta__3", "beta__4"]}

    coefdiffs = {}
    probdiffs = {}
    for name, (coef1, coef2) in diffs.items():
        coefdiffs[name] = samples[coef1].values - samples[coef2].values
        prob1 = norm.cdf(samples["beta__1"].values + samples[coef1].values)
        prob2 = norm.cdf(samples["beta__1"].values + samples[coef2].values)
        probdiffs[name] = prob1 - prob2

    coef_df = pd.DataFrame(coefdiffs)
    coef_intervals = mcmcstats.interval_summary(coef_df)

    print("\n\nCoefficients")
    print(coef_intervals.to_string())

    prob_df = pd.DataFrame(probdiffs)
    prob_intervals = mcmcstats.interval_summary(prob_df)

    print("\n\nProbability at 1 predictor unit")
    print(prob_intervals.to_string())
