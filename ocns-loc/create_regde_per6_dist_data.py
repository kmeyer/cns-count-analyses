#!/usr/bin/env python3

import numpy as np
import pandas as pd

import rdump

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")

period_idx = 5

cols = ["ocns_intronic", "ocns_near", "ocns_far"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts-dist.csv")
ng = ng[cols]
ng = np.log1p(ng)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values])
data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data, "dp__regde_per6_dist.data.r")
