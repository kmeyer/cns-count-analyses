#!/usr/bin/env python3

import json

import pandas as pd

period = 6
period_idx = period - 3

with pd.HDFStore("../data/expression/kang2011spatio-p1.h5") as store:
    gi = store["geneinfo"]

with open("../data/gencode/gencode-v19-locus-lengths.json") as jfh:
    lens = json.load(jfh)
lens = {k.split("|")[1]: v for k, v in lens.items()}

gi.ensg.map(lens).to_csv("kang2011spatio-loclens.csv")
