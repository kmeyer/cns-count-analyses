#!/usr/bin/env python3

import json
import sys

import numpy as np
import pandas as pd

import rdump

try:
    period = int(sys.argv[1])
except (ValueError, IndexError):
    sys.exit("Usage: {} PERIOD".format(sys.argv[0]))
period_idx = period - 1

with pd.HDFStore("../data/expression/kang2011spatio-p1.h5") as store:
    gi = store["geneinfo"]

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")

cols = ["ocns", "hacns", "cacns", "macns"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng = ng[cols]
ng = np.log1p(ng)

with open("../data/gencode/gencode-v19-locus-lengths.json") as jfh:
    lens = json.load(jfh)
lens = {k.split("|")[1]: v for k, v in lens.items()}

bm = gi["ensg"].isin(lens).values
ensgs = gi["ensg"][bm]
de = de[bm]
ng = ng[bm]

loclens = np.log(ensgs.map(lens).values)
loclens = (loclens - np.mean(loclens)) / np.std(loclens)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values,
               loclens[:, None]])

data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data, "dp__regde_loclen_period{}.data.r".format(period))
