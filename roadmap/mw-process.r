#!/usr/bin/env Rscript

library(data.table)

load("mw-test-results.rdata")

df <- rbindlist(results)

df[, fname := as.character(fname)]
fname.splits <- do.call(rbind, strsplit(df[, fname], "-", fixed=TRUE))
df$samp <- fname.splits[,1]
df$mark <- fname.splits[,2]

df[, fname := NULL]

comp.order <- as.character(df[,unique(comp)])

df[, comp := factor(comp, levels=comp.order)]

df[, pvals.bonf := p.adjust(pvals, method="bonferroni")]

is.brain <- c("E067", "E068", "E069", "E070", "E071", "E072", "E073",
              "E074", "E081", "E082")

is.fetal <- c("E070", "E081", "E082", "E083", "E086", "E088", "E089",
              "E090")

df[, brain := samp %in% is.brain]
df[, fetal := samp %in% is.fetal]

df[, bf := paste(as.character(as.numeric(brain)),
                 as.character(as.numeric(fetal)),
                 sep="")]

df[, group := sapply(bf,
                     function (x)
                         switch(x,
                                "00" = "adult_other",
                                "10" = "adult_brain",
                                "01" = "fetal_other",
                                "11" = "fetal_brain"))]

df[, group := factor(group, levels=c("fetal_brain", "fetal_other",
                                     "adult_brain", "adult_other"))]

df[, brain := NULL]
df[, fetal := NULL]

save(df, file="mw-test-results-process.rdata")
