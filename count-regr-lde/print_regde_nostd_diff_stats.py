#!/usr/bin/env python3

import numpy as np
import pandas as pd
from scipy.stats import norm

import mcmcstats
import stanio

samps = stanio.read_samples("samples/dp__regde_nostd_ragg_period6-all-chains.j4.csv",
                            filter_svars=True)

print("\n\nCoefficients")
print(mcmcstats.interval_summary(samps))

mean_ragg = np.mean(np.loadtxt("../data/nearest-gene/kang2011spatio-randagg.dat"))

n_pred = 10
n_pred_l = np.log1p(n_pred)

base = samps["beta__1"].values + mean_ragg * samps["beta__6"]
p0 = norm.cdf(base)
p1 = norm.cdf(base + n_pred_l * samps["beta__2"].values)

print("\n\nProbability at 10 predictor units for OCNS".format(n_pred))
dint = mcmcstats.interval_summary(pd.Series(p1 - p0))
print(dint.to_string())
