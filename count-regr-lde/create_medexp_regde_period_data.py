#!/usr/bin/env python3
"""Like create_regde_period_data.py, but include expression predictor.
"""
import sys

import numpy as np
import pandas as pd

import rdump

try:
    period = int(sys.argv[1])
except (ValueError, IndexError):
    sys.exit("Usage: {} PERIOD".format(sys.argv[0]))
period_idx = period - 1

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")

cols = ["ocns", "hacns", "cacns", "macns"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng = ng[cols]
ng = np.log1p(ng)

mexp = np.loadtxt("../data/expression/"
                  "kang2011spatio-p1-region-std-median-per-stage.dat")

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values,
               mexp[:, period_idx, None]])

data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data, "dp__regde_medexp_period{}.data.r".format(period))
