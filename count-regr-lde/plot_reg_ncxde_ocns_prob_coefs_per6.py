#!/usr/bin/env python3

from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import norm

import mplutil
import mcmcstats
from plots import pointrange
import stanio
import colors

mplutil.style_use(["latex"])

rsamps = stanio.read_samples("samples/dp__regde_period6-all-chains.j4.csv",
                             filter_svars=True)
nsamps = stanio.read_samples("samples/dp__ncxde_period6-all-chains.j4.csv",
                             filter_svars=True)

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv",
                 usecols=["ocns", "hacns", "cacns", "macns"])
ng = np.log1p(ng)

ng_noacns = ng[ng[["hacns", "cacns", "macns"]].sum(1) == 0]
ocns_max = ng_noacns["ocns"].max()
ocns_vals = np.linspace(ng["ocns"].min(), ocns_max)

np.random.seed(2182348196)
nreals = 100
reals_idx = np.sort(np.random.choice(rsamps.index, nreals, replace=False))
rsamps_reals = rsamps.iloc[reals_idx]
nsamps_reals = nsamps.iloc[reals_idx]

npreds = ng.shape[1] + 1

preds_base = np.zeros((len(ocns_vals), npreds))
preds_base[:, 0] = 1.
preds_ocns = preds_base.copy()
preds_ocns[:, 1] = ocns_vals

cols = ["beta__{}".format(i) for i in range(1, npreds + 1)]

prob_ocns = {}
for kind, reals in zip(["reg", "ncx"], [rsamps_reals, nsamps_reals]):
    linpred_ocns = np.inner(reals[cols].values,
                            preds_ocns)
    prob_ocns[kind] = norm.cdf(linpred_ocns)

fig = plt.figure(figsize=(mplutil.onecol_width * 0.85, 3.25))
gs = gridspec.GridSpec(2, 1, height_ratios=[2, 1])

alpha = 0.2
regcolor, ncxcolor = colors.primary, colors.secondary

## Probability plot

ax_ocns = fig.add_subplot(gs[0])

ax_ocns.plot(ocns_vals, prob_ocns["reg"].T,
             color=regcolor, alpha=alpha)
ax_ocns.plot(ocns_vals, prob_ocns["ncx"].T,
             color=ncxcolor, alpha=alpha)

ax_ocns.set_ylabel("Probabiltiy of\ndifferential expression")
ax_ocns.set_xlabel(r"$\ln(1 + \text{Number of OCNSs})$")

ax_ocns.text(1.0, 0.19, "Brain regions",
             va="center", ha="center",
             color=regcolor, fontsize="small")
ax_ocns.text(3.5, 0.2, "Neocortical\nareas",
             va="center", ha="left",
             color=ncxcolor, fontsize="small")

ax_ocns.set_ylim(0, 1)
ax_ocns.set_xlim(0, ocns_max + 0.1)
mplutil.remove_spines(ax_ocns)

## Coefficient plot

ax_coef = fig.add_subplot(gs[1])

coefs = {"reg": mcmcstats.interval_summary(rsamps),
         "ncx": mcmcstats.interval_summary(nsamps)}

coef_cols =  ["beta__{}".format(idx) for idx in range(2, 6)]

offset = {"reg": -0.1, "ncx": 0.1}
kcolors = {"reg": colors.primary, "ncx": colors.secondary}
for kind in ["reg", "ncx"]:
    coef_df = coefs[kind].loc[coef_cols]
    pointrange(np.arange(coef_df.shape[0]) + offset[kind],
               coef_df["mean"],
               coef_df["p025"], coef_df["p975"],
               color=kcolors[kind],
               mec="none",
               markersize=4, ax=ax_coef)

mplutil.remove_spines(ax_coef)
ax_coef.locator_params(axis="y", nbins=4)

ax_coef.axhline(0, ls="--", color=colors.base, zorder=1)
ax_coef.set_ylabel("Coefficient value", labelpad=2)

ax_coef.set_xticks(np.arange(0, 4))
ax_coef.set_xticklabels(["OCNS", "HACNS", "CACNS", "MACNS"])

ax_coef.set_xlim(-0.5, 3.5)

fig.subplots_adjust(left=0.22, right=0.98, bottom=0.05, top=0.955,
                    hspace=0.5)
print("h/w: {}".format(ax_ocns.bbox.height / ax_ocns.bbox.width))

xpos, _ = mplutil.subplot_label(ax_ocns, r"\textbf{A}")
mplutil.subplot_label(ax_coef, r"\textbf{B}", xpos=xpos)

fig.savefig("reg-ncxde-per6-prob-coefs.pdf")
