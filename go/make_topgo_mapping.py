#!/usr/bin/env python3
"""Make topGO mapping from gene association file.

Usage: make_topgo_mapping.py FILE
       make_topgo_mapping.py --help

Arguments:
  FILE
      GO gene association file (compressed)
"""

from collections import defaultdict
import gzip
from docopt import docopt

args = docopt(__doc__)

goterms = defaultdict(set)
with gzip.open(args["FILE"], "rt") as fh:
    for line in fh:
        if line.startswith("!"):
            continue
        fields = line.split("\t")
        gene_name, goterm = fields[2], fields[4]
        goterms[gene_name].add(goterm)

for gene in sorted(goterms):
    goline = ", ".join(list(sorted(goterms[gene])))
    print("{}\t{}".format(gene, goline))
