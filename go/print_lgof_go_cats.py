#!/usr/bin/env python3

import sys
import pandas as pd

try:
    elem = sys.argv[1]
except (ValueError, IndexError):
    sys.exit("Usage: {} {gof,lof}".format(sys.argv[0]))

for per in range(13, 16):
    print("\nPeriod {}".format(per))
    gd = pd.read_csv("{}-cortex-up-go-bp-period{}.csv".format(elem, per),
                     index_col=0)
    od = pd.read_csv("ocns-cortex-up-go-bp-period{}-thresh10.csv".format(per),
                     index_col=0)
    rd = pd.read_csv("ragg-cortex-up-go-bp-period{}-thresh10.csv".format(per),
                     index_col=0)

    or_cats = pd.concat([od["GO.ID"], rd["GO.ID"]]).unique()

    gd_only = gd[~gd["GO.ID"].isin(or_cats)]

    print("{} table, {}".format(elem, gd.shape[0]))
    print(gd.to_string())

    print("{}-only table, {}".format(elem, gd_only.shape[0]))
    print(gd_only.to_string())
