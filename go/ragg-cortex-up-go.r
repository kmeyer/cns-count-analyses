#!/usr/bin/env Rscript

args <- commandArgs(trailingOnly = TRUE)
if (length(args) == 0){
    stop("Usage: ./ragg-cortex-up-go.r {mf,bp,cc} PERIOD threshold")
} else{
    ontology <- args[1]
    period <- args[2]
    period.idx <- as.numeric(period) - 12
    thresh <- as.numeric(args[3])
}

library("topGO")

ng = scan("../data/nearest-gene/gtex-randagg.dat", quiet=TRUE)
de = read.csv("../gtex/tissue_nomerge-de-full-up.csv")

## tissues = c("cerebellum",
##             "cortex",
##             "heart",
##             "kidney",
##             "liver",
##             "lung",
##             "muscle",
##             "ovary",
##             "pancreas",
##             "spleen",
##             "testis")

cort.up.cols <- paste("pr", period.idx, ".2...pr", period.idx, ".",
                      3:11, sep="")

de.cort.up <- de[,cort.up.cols]
all.cort.up <- apply(de.cort.up, 1, all)

gnames <- scan("../data/expression/gtex-gene-names.txt",
               what="character", quiet=TRUE)
gomap <- readMappings("go-human-genename-mapping.txt")

near.elem <- as.numeric(ng > log1p(thresh))[all.cort.up]
names(near.elem) <- gnames[all.cort.up]

gd <- new("topGOdata", ontology=toupper(ontology),
          allGenes=factor(near.elem),
          annot=annFUN.gene2GO,
          gene2GO=gomap,
          nodeSize=10)

test.f <- runTest(gd, statistic="fisher")

top <- GenTable(gd, fisher=test.f,
                topNodes=sum(score(test.f) < 0.01))

write.csv(top,
          file=paste("ragg-cortex-up-go-", ontology,
                     "-period", period,
                     "-thresh", thresh,
                     ".csv",
                     sep=""))
