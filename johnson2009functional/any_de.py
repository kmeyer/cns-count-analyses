#!/usr/bin/env python3
"""Collapse DE for contrasts.

Usage: any_de.py <de-file> <outfile>
"""

from docopt import docopt
import numpy as np
import pandas as pd

args = docopt(__doc__)
defile, outfile = args["<de-file>"], args["<outfile>"]

de = pd.read_csv(defile)
de.index.name = "gene"
de_any = de.abs().any(1).astype(np.int)
de_any.to_csv(outfile, sep=" ", index=False, header=False)
