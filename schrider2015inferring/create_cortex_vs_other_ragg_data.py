#!/usr/bin/env python3

import sys

import h5py
import numpy as np
import pandas as pd

import rdump

try:
    tiss = sys.argv[1]
except (ValueError, IndexError):
    sys.exit("Usage: {} TISSUE".format(sys.argv[0]))

tissues = ["cerebellum",
           "cortex",
           "heart",
           "kidney",
           "liver",
           "lung",
           "muscle",
           "ovary",
           "pancreas",
           "spleen",
           "testis"]
upcol = "pr1.2 - pr1.{}".format(tissues.index(tiss) + 1)

de = pd.read_csv("../gtex/tissue_nomerge-de-full-up.csv")[upcol].values

cols = ["lof", "gof", "ocns_nolgof"]
ng = pd.read_csv("../data/nearest-gene/gtex-nearest-counts-lgof.csv")
ng = ng[cols]
ng = np.log1p(ng)

ragg = np.loadtxt("../data/nearest-gene/gtex-randagg.dat")
ragg = (ragg - np.mean(ragg)) / np.std(ragg)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values,
               ragg[:, None]])

data = {}
data["y"] = de
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data, "dp__cortex_vs_{}.data.r".format(tiss))
