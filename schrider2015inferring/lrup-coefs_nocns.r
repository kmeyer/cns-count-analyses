#!/usr/bin/env Rscript

library(rhdf5)

de <- as.matrix(read.csv("../gtex/tissue_nomerge-de-full-up.csv"))

cols <- c("lof", "gof")
ng <- read.csv("../data/nearest-gene/gtex-nearest-counts-lgof.csv")
ng <- ng[cols]
ng <- log1p(ng)

periods <- unlist(lapply(strsplit(colnames(de), ".", fixed=TRUE),
                         function (x) x[1]))
periods <- as.numeric(sub("pr", "", periods, fixed=TRUE))

fits <- lapply(1:dim(de)[2],
               function (n)
                   fit <- glm(de[,n] ~ ng$lof + ng$gof,
                              family=binomial(link="probit")))

coefs <- data.frame(t(sapply(fits, coef)))
colnames(coefs) <- c("intercept", "lof", "gof")
coefs$contrast <- gsub("...", "-", colnames(de), fixed=TRUE)

write.csv(coefs, file="tissue_nomerge_nocns-coefs-up.csv",
          row.names=FALSE, quote=FALSE)
