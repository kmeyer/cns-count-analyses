#!/usr/bin/env Rscript

d <- read.csv("../data/nearest-gene/gtex-nearest-counts-lgof.csv")
v <- read.csv("../data/nearest-gene/gtex-nearest-counts-lgof-vert100.csv")

print(cor(d$ocns_nolgof, v$ocns_nolgof, method="spearman"))
