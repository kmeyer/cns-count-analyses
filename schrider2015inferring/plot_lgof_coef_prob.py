#!/usr/bin/env python3

from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import norm

import colors
import mcmcstats
import mplutil
import periodax
from plots import pointrange
import stanio

mplutil.style_use(["latex"])

ss_cns = stanio.read_samples("samples/dp__regde_ragg-all-chains-all-periods.j4.csv",
                           filter_svars=True)

ss = stanio.read_samples("samples/dp__regde_nocns-all-chains-all-periods.j4.csv",
                         filter_svars=True)

fig = plt.figure(figsize=(mplutil.twocol_width * 0.99, 5.5))
fig.subplots_adjust(left=0.096, right=0.973, bottom=0.115, top=0.968,
                    wspace=0.2, hspace=0.1)

gs = gridspec.GridSpec(3, 1,
                       height_ratios=[1, 0.2, 0.45])

gs0 = gridspec.GridSpecFromSubplotSpec(2, 2, subplot_spec=gs[0],
                                       wspace=0.25, hspace=0.1,
                                       width_ratios=[1, 0.5])

gs01 = gridspec.GridSpecFromSubplotSpec(3, 2, subplot_spec=gs0[:, 1],
                                        hspace=0.1, wspace=0.1)

gs1 = gridspec.GridSpecFromSubplotSpec(1, 6, subplot_spec=gs[2],
                                       wspace=0.05,
                                       width_ratios=[1, 1, 1, 1, 0.2, 0.04])

marker_size = 4.5

## Coefficient plot

ax_coef = fig.add_subplot(gs0[0, 0])
ax_coef_cns = fig.add_subplot(gs0[1, 0])

offsets = {"lof": -0.2, "gof": 0.2}
pcolors = {"lof": colors.base, "gof": colors.primary}
beta_idx = {"lof": "beta__2", "gof": "beta__3"}

for ax, df in zip([ax_coef, ax_coef_cns], [ss, ss_cns]):
    for param in ["lof", "gof"]:
        param_cols = gof = [c.endswith(beta_idx[param]) for c in df.columns]
        param_intervals = mcmcstats.interval_summary(df.loc[:, param_cols])
        pointrange(np.arange(1, 16) + offsets[param],
                   param_intervals["mean"],
                   param_intervals["p025"], param_intervals["p975"],
                   color=pcolors[param],
                   mec="none", zorder=2,
                   markersize=marker_size, ax=ax)
    ax.set_ylim(-1.2, 1.2)
    ax.axhline(0, color=colors.light_gray, ls="--", zorder=1)
    periodax.period_vlines(ax, start=1)
    mplutil.remove_spines(ax)
    ax.set_xlim(0.5, 15.6)
    ax.tick_params(axis="x", length=0)
    ax.locator_params(axis="y", nbins=6)

ax_coef.text(13.6, -0.35, "ncLOF", ha="left",
             fontsize="medium", color=pcolors["lof"])
ax_coef.text(13.6, -0.60, "ncGOF", ha="left",
             fontsize="medium", color=pcolors["gof"])

ax_coef.set_xticks([])
ax_coef_cns.set_xticks(np.arange(1, 16))


ax_coef.set_title("Differential expression among brain regions",
                  fontsize="medium")

ax_coef.text(0.9, -1.0, "No adjustment",
             fontsize="medium", ha="left", va="center")
ax_coef_cns.text(0.9, -1.0, "OCNS and target size adjustment",
                 fontsize="medium", ha="left", va="center")

## Probability plot

ss_cv = stanio.read_samples("samples/dp__cortex_vs-all-chains-all-other.j4.csv",
                            filter_svars=True)

ax_probs = np.empty((3, 2), dtype="O")
for r in range(3):
    for c in range(2):
        ax_probs[r, c] = fig.add_subplot(gs01[r, c])

xvals_nolog = np.arange(1, 4)
xvals = np.log1p(xvals_nolog)
preds_base = np.zeros((len(xvals), 5))
preds_base[:, 0] = 1.
preds = preds_base.copy()
preds[:, 2] = xvals

sel_tissues = ["cerebellum", "heart", "liver",
               "muscle", "pancreas", "testis"]

for ax, tis in zip(ax_probs.ravel(), sel_tissues):
    col_bm = [c.startswith(tis) for c in ss_cv.columns]
    linpred_base = np.inner(ss_cv.loc[:, col_bm].values,
                            preds_base)
    linpred = np.inner(ss_cv.loc[:, col_bm].values, preds)
    probdiff = norm.cdf(linpred) - norm.cdf(linpred_base)
    pdiff_interval = mcmcstats.interval_summary(pd.DataFrame(probdiff))

    pointrange(xvals_nolog,
               pdiff_interval["mean"],
               pdiff_interval["p025"], pdiff_interval["p975"],
               color="#006400", zorder=2,
               mec="none", markersize=marker_size, ax=ax)
    ax.axhline(0, color=colors.light_gray, ls="--", zorder=1)

    ax.text(0.5, 0.9, "vs {}".format(tis),
            fontsize="small", ha="center",
            transform=ax.transAxes)

    mplutil.remove_spines(ax)

for ax in ax_probs.ravel():
    ax.set_ylim(-0.05, 0.3)
    ax.set_xticks(xvals_nolog)
    ax.locator_params(axis="y", nbins=4)

for ax in ax_probs[:-1, :].ravel():
    ax.set_xticklabels([])
for ax in ax_probs[:, 1]:
    ax.set_yticklabels([])

## GTEx heat maps

de = pd.read_csv("tissue_nomerge_nocns-coefs-up-names.csv")
de = de[de["period"] == 1]

de_cns = pd.read_csv("tissue_nomerge-coefs-up-names.csv")
de_cns = de_cns[de_cns["period"] == 1]

heat_params = ["lof", "gof"]
max_val_de = max(abs(de[heat_params].values.min()),
                 de[heat_params].values.max())
max_val_de_cns = max(abs(de_cns[heat_params].values.min()),
                     de_cns[heat_params].values.max())
max_val = max(max_val_de, max_val_de_cns)
vmin, vmax = -max_val, max_val

haxs = []

mats = [("lof", de), ("lof", de_cns),
        ("gof", de), ("gof", de_cns)]
labels = ["ncLOF", "ncLOF, adjusted",
          "ncGOF", "ncGOF, adjusted"]

ims = []

for (param, mat), col in zip(mats, range(4)):
    tissues = mat["rlabel1"].unique()

    dem = mat.set_index(["region1", "region2"])
    dem = dem[param].unstack("region1")

    hax = fig.add_subplot(gs1[0, col])
    haxs.append(hax)

    vals = dem.values
    im = hax.imshow(vals,
                    cmap=colors.cmap_div,
                    vmin=vmin, vmax=vmax,
                    interpolation="none", aspect="auto")
    ims.append(im)
    hax.tick_params(length=0)

    hax.set_xticks(np.arange(dem.shape[1]))
    hax.set_yticks(np.arange(dem.shape[0]))

    hax.set_xticklabels(tissues[dem.columns.values],
                        fontsize="small", rotation=90)
    if col == 0:
        hax.set_yticklabels(tissues[dem.index.values], fontsize="small")
    else:
        hax.set_yticklabels([])

    mplutil.style_heatmap(hax)

for ax, lab in zip(haxs, labels):
    ax.set_title(lab, fontsize="medium")

cax = fig.add_subplot(gs1[0, 5])
mplutil.heat_colorbar(cax, ims[-1], "Coefficient value")

########################################################################

_, ypos = mplutil.subplot_label(ax_probs[0, 0], r"\textbf{C}", xpad=15)
xpos, _ = mplutil.subplot_label(ax_coef, r"\textbf{A}",
                                ypos=ypos, xpad=15)
mplutil.subplot_label(haxs[0], r"\textbf{B}", xpos=xpos)


periodax.label_groups(ax_coef_cns, start=1)
mplutil.shared_ylabel([ax_coef, ax_coef_cns], "Coefficient value")
mplutil.shared_ylabel(ax_probs[:, 0],
                      "Increase in probability of upregulation in cortex")
mplutil.shared_xlabel(ax_probs[-1, :], "Number of ncGOFs")

fig.savefig("lgof-coef-prob.pdf")
