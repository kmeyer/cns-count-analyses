#!/usr/bin/env python3

import pandas as pd

upcol = "pr1.2 - pr1.{}".format(11)
de = pd.read_csv("../gtex/tissue_nomerge-de-full-up.csv")[upcol]

cols = ["lof", "gof", "ocns_nolgof"]
ng = pd.read_csv("../data/nearest-gene/gtex-nearest-counts-lgof.csv")
ng = ng[cols]

print("Overall DE proportion")
print(de.mean())

print("\n\nDE proportion for genes near a ncGOF")
print(de.groupby(ng["gof"] > 0).size().to_string())
print("")
print(de.groupby(ng["gof"] > 0).mean().to_string())

print("\n\nDE proportion for genes near N ncGOFs")
print(de.groupby(ng["gof"]).size().to_string())
print("")
print(de.groupby(ng["gof"]).mean().to_string())
