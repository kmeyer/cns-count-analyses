#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import mplutil
from plots import pointrange
import colors

mplutil.style_use(["latex"])

dd = pd.read_csv("testis-coefs.csv")
dd = dd.drop("name", axis=1)

tissues = dd["reg2"].unique()

dg = dd.groupby(["param", "period"])

fig, axs = plt.subplots(ncols=1, nrows=dd["period"].nunique(),
                        sharey=True, sharex=True,
                        figsize=(0.7 * mplutil.onecol_width, 3.0))

xs = np.arange(dd["reg2"].nunique())

for ax, period in zip(axs, range(13, 16)):
    dh = dg.get_group(("hacns", period - 12))

    assert np.all(dh["reg2"] == tissues)

    ax.axhline(0, ls="--", color=colors.light_gray)
    pointrange(xs, dh["mean"], dh["lb"], dh["ub"],
               color=colors.base,
               mec="none",
               markersize=4, ax=ax)

    ax.tick_params(axis="x", length=0)
    ax.locator_params(axis="y", nbins=4)

    ax.text(0.5, 0.98, "Period {}".format(period),
            fontsize="small", ha="center", va="center",
            transform=ax.transAxes)

    mplutil.remove_spines(ax)

axs[-1].set_xticks(xs)
axs[-1].set_xticklabels(tissues,
                        fontsize="medium",
                        rotation="vertical")

fig.subplots_adjust(left=0.23, right=0.99, bottom=0.25, top=0.965,
                    wspace=0.2, hspace=0.3)

mplutil.shared_ylabel(axs, "HACNS coefficient")

fig.savefig("testis-coefs.pdf")
