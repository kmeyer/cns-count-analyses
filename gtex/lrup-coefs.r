#!/usr/bin/env Rscript

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 2){
    stop("usage: ./lrup-coefs.r NAME OUTFILE")
} else{
    name <- args[1]
    outfile <- args[2]
}

de <- as.matrix(read.csv(paste(name, "-de-full-up.csv", sep="")))

ng <- read.csv("../data/nearest-gene/gtex-nearest-counts.csv")
ng <- ng[c("ocns", "hacns", "cacns", "macns")]
ng <- log1p(ng)
ragg <- scan("../data/nearest-gene/gtex-randagg.dat", quiet=TRUE)
ng$ragg <- ragg

periods <- unlist(lapply(strsplit(colnames(de), ".", fixed=TRUE),
                         function (x) x[1]))
periods <- as.numeric(sub("pr", "", periods, fixed=TRUE))

fits <- lapply(1:dim(de)[2],
               function (n)
                   fit <- glm(de[,n] ~ ng$ocns +
                                  ng$hacns + ng$cacns + ng$macns + ragg,
                              family=binomial(link="probit")))

coefs <- data.frame(t(sapply(fits, coef)))
colnames(coefs) <- c("intercept", "ocns",
                     "hacns", "cacns", "macns", "ragg")
coefs$contrast <- gsub("...", "-", colnames(de), fixed=TRUE)

write.csv(coefs, file=outfile, row.names=FALSE, quote=FALSE)
