#!/usr/bin/env python3

import matplotlib as mpl
from matplotlib import gridspec
from matplotlib import ticker
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

de = pd.read_csv("tissue_nomerge-coefs-up-names.csv")
tissues = de["rlabel1"].unique()
de = de[de["period"] == 1]

dem = de.set_index(["region1", "region2"])
dem = dem["ocns"].unstack("region1")

fig = plt.figure(figsize=(mplutil.onecol_width * 0.74, 2.8))
gs = gridspec.GridSpec(2, 1,
                       height_ratios=[1, 1.7])

max_val = max(abs(de["ocns"].min()), de["ocns"].max())

vmin, vmax = -max_val * 1.07, max_val * 1.07

hax = fig.add_subplot(gs[1, 0])

vals = dem.values
im = hax.imshow(vals,
                cmap=colors.cmap_div,
                vmin=vmin, vmax=vmax,
                interpolation="none", aspect="auto")

hax.tick_params(length=0)

hax.set_yticks(np.arange(dem.shape[0]))
hax.set_xticks(np.arange(dem.shape[1]))

hax.set_xticklabels(tissues[dem.columns.values],
                    fontsize="small", rotation=90)
hax.set_yticklabels(tissues[dem.index.values], fontsize="small")

mplutil.style_heatmap(hax)

## Box

rgroups = de.groupby("region1")

bax = fig.add_subplot(gs[0, 0])

vals = [de.loc[rgroups.groups[r], "ocns"].values
        for r in dem.columns]

elems = bax.boxplot(vals, showcaps=False,
                    patch_artist=True,
                    widths=0.75,
                    sym=".",
                    flierprops={"markersize": 4,
                                "alpha": 0.9,
                                "markeredgecolor": "none",
                                "markerfacecolor": colors.base})

for elem_type, elem in elems.items():
    if elem_type == "boxes":
        for artist in elem:
            artist.set_facecolor(colors.face)
            artist.set_edgecolor(colors.base)
    else:
        for artist in elem:
            artist.set_color(colors.base)

bax.set_xticks([])
bax.tick_params(axis="y", pad=25, labelsize="medium", direction="in")
mplutil.remove_spines(bax, which=["top", "right", "bottom", "left"])
bax.set_yticklabels([])

assert de["ocns"].max() < 0.25

bax.set_ylabel("OCNS\ncoefficient")

bax.axhline(0, ls="--", lw="0.5", color=colors.base,
            alpha=0.5, zorder=1)

fig.subplots_adjust(left=0.28, right=0.99, bottom=0.227, top=0.975,
                    wspace=0.06, hspace=0.12)

## Color bar

cbar_coords = bax.transAxes.transform([(-0.03, 0),
                                       (-0.00, 1)])
cbar_coords = fig.transFigure.inverted().transform(cbar_coords)
cax = plt.axes([cbar_coords[0, 0], cbar_coords[0, 1],
                cbar_coords[1, 0] - cbar_coords[0, 0],
                cbar_coords[1, 1] - cbar_coords[0, 1]])

cbar = fig.colorbar(im, cax=cax)
cbar.ax.tick_params(labelsize="small", length=1)
cbar.ax.yaxis.set_ticks_position("left")
cbar.locator = ticker.MaxNLocator(nbins=5)
cbar.update_ticks()
cbar.outline.set_linewidth(mpl.rcParams["patch.linewidth"])

bax.set_ylim(*cbar.get_clim())

fig.savefig("tissue_nomerge-heat-box.pdf")
