#!/usr/bin/env python3
"""Fill in low-counts filtered before limma call.

Usage: fill_low.py <hdf5> <de-file> <outfile>
"""

import h5py
from docopt import docopt
import numpy as np
import pandas as pd

args = docopt(__doc__)

with h5py.File(args["<hdf5>"]) as hf:
    pass_lowfilter = hf["pass_lowfilter"][:]

de_filtered = pd.read_csv(args["<de-file>"])

de = np.zeros((len(pass_lowfilter), de_filtered.shape[1]), dtype=np.int)
de[pass_lowfilter] = de_filtered.values
de = pd.DataFrame(de, columns=de_filtered.columns)

de.to_csv(args["<outfile>"], index=False)
