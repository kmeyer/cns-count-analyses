#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

dd = pd.read_csv("gam-mexp-regde-log1p-smooth-data.csv")

fig, axs = plt.subplots(ncols=2, sharey=True,
                        figsize=(mplutil.twocol_width * 0.85, 2.1),
                        squeeze=False)

params = dd["x.var"].str.replace(r"log1p\(", "").str.replace(r"\)", "")

pnames = ["ocns", "mexp"]

for pname, ax in zip(pnames, axs.ravel()):
    ddp = dd[params == pname]

    ax.plot(ddp["x.val"], ddp["value"], color=colors.base)

    ax.fill_between(ddp["x.val"],
                    ddp["value"] - 2 * ddp["se"],
                    ddp["value"] + 2 * ddp["se"],
                    color=colors.face)

    mplutil.remove_spines(ax)
    ax.axhline(0, ls="--", color=colors.light_gray, zorder=1)

    ax.locator_params(axis="x", nbins=4)

fig.subplots_adjust(left=0.1, right=0.98, bottom=0.18, top=0.96,
                    wspace=0.2, hspace=0.45)

axs[0, 0].set_ylabel("Linear contribution")
axs[0, 0].set_xlabel(r"$\ln(1 + \text{Number of OCNSs})$")
axs[0, 1].set_xlabel(r"Median expression (standardized)")


fig.savefig("gam-mexp-regde-log1p.pdf")
