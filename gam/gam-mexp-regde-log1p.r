#!/usr/bin/env Rscript

library("mgcv")

source("gam_smooth.r")

de <- as.matrix(read.table("../limma-de/region-de-p1.dat", sep=" "))
colnames(de) <- NULL

period.idx <- 6

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng <- ng[c("ocns", "hacns", "cacns", "macns")]

mexp.file <- "../data/expression/kang2011spatio-p1-region-std-median-per-stage.dat"
medexp <- as.matrix(read.table(mexp.file, sep=" "))
colnames(medexp) <- NULL

dat <- data.frame(de=de[,period.idx], ng, mexp=medexp[,period.idx])


fit.gam <- gam(de ~ s(log1p(ocns)) +
                   s(log1p(hacns), k=7) + s(log1p(cacns), k=7) + s(log1p(macns)) +
                   s(mexp),
               family=binomial(link="probit"), data=dat)

gsd <- gam_smooth_data(fit.gam)

write.csv(gsd$dat, file="gam-mexp-regde-log1p-smooth-data.csv",
          row.names=FALSE)
write.csv(t(gsd$edf), file="gam-mexp-regde-log1p-smooth-edf.csv", row.names=FALSE)
