#!/usr/bin/env Rscript

library("mgcv")

source("gam_smooth.r")

de <- as.matrix(read.table("../limma-de/region-de-p1.dat", sep=" "))
colnames(de) <- NULL

period.idx <- 6

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng <- ng[c("ocns", "hacns", "cacns", "macns")]

dat <- data.frame(de=de[,period.idx], ng)


fit.gam <- gam(de ~ s(log1p(ocns)) +
                   s(hacns, k=7) + s(cacns, k=7) + s(macns),
               family=binomial(link="probit"), data=dat)

gsd <- gam_smooth_data(fit.gam)

write.csv(gsd$dat, file="gam-regde-nolog-acns-smooth-data.csv",
          row.names=FALSE)
write.csv(t(gsd$edf), file="gam-regde-nolog-acns-smooth-edf.csv", row.names=FALSE)
