#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

dd = pd.read_csv("gam-regde-nolog-acns-smooth-data.csv")

fig, axs = plt.subplots(ncols=2, nrows=2, sharey=True,
                        figsize=(mplutil.twocol_width * 0.85, 3.8))

params = dd["x.var"].str.replace(r"log1p\(", "").str.replace(r"\)", "")

pnames = ["ocns", "hacns", "cacns", "macns"]

for pname, ax in zip(pnames, axs.ravel()):
    ddp = dd[params == pname]

    ax.plot(ddp["x.val"], ddp["value"], color=colors.base)

    ax.fill_between(ddp["x.val"],
                    ddp["value"] - 2 * ddp["se"],
                    ddp["value"] + 2 * ddp["se"],
                    color=colors.face)

    if pname == "ocns":
        ax.set_xlabel(r"$\ln(1 + \text{{Number of OCNSs}})$")
    else:
        ax.set_xlabel("Number of {}s".format(pname.upper()))

    mplutil.remove_spines(ax)
    ax.axhline(0, ls="--", color=colors.light_gray, zorder=1)

    ax.locator_params(axis="x", nbins=4)

fig.subplots_adjust(left=0.1, right=0.98, bottom=0.1, top=0.96,
                    wspace=0.2, hspace=0.45)

mplutil.shared_ylabel(axs[:, 0], "Linear contribution")

fig.savefig("gam-regde-nolog-acns.pdf")
