#!/usr/bin/env python3

import numpy as np
import pandas as pd

ng = pd.read_csv("../data/nearest-gene/lambert2011genes-nearest-counts.csv",
                 usecols=["cns", "hacns", "cacns", "macns"])
ngb = (ng > 0).astype(np.int)

is_near_cns = ngb["cns"] == 1

print("\n\nAll genes\n")
print(ngb[is_near_cns].sum().to_string())
print("")
print(ngb[is_near_cns].mean().to_string())

# gene x period
is_de = np.loadtxt("../lambert2011genes/region-de.dat",
                   dtype=np.bool)

print("\n\nRegionally DE genes\n")
print(ngb[is_near_cns & is_de].sum().to_string())
print("")
print(ngb[is_near_cns & is_de].mean().to_string())
