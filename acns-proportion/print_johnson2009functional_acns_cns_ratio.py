#!/usr/bin/env python3

import sys

import numpy as np
import pandas as pd

try:
    _, kind = sys.argv
except ValueError:
    sys.exit("Usage: {} KIND".format(sys.argv[0]))

ng = pd.read_csv("../data/nearest-gene/johnson2009functional-nearest-counts.csv",
                 usecols=["cns", "hacns", "cacns", "macns"])
ngb = (ng > 0).astype(np.int)

is_near_cns = ngb["cns"] == 1

print("\n\nAll genes\n")
print(ngb[is_near_cns].sum().to_string())
print("")
print(ngb[is_near_cns].mean().to_string())

# gene x period
is_de = np.loadtxt("../johnson2009functional/{}-de.dat".format(kind),
                   dtype=np.bool)

print("\n\nDE genes ({})\n".format(kind))
print(ngb[is_near_cns & is_de].sum().to_string())
print("")
print(ngb[is_near_cns & is_de].mean().to_string())
