
pair.diffs <- function (xs){
    ## Return a vector of strings comprised of contrasts for each pair
    ## of elements in xs.
    ##
    ##     > pair.diffs(c(1, 2, 3))
    ##     [1] "1 - 2" "1 - 3" "2 - 3"
    apply(t(combn(xs, 2)), 1,
          function(x) paste(x[1], x[2], sep=" - "))
}
