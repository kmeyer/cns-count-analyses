#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd

import colors
import mplutil
from plots import pointrange

mplutil.style_use(["latex"])

params = ["ocns", "hacns"]

methods = ["lm", "lm_llen"]
fnames = ["lm-regde-score.csv", "lm-regde-score-ragg.csv"]

dfs = []
for method, fname in zip(methods, fnames):
    df = pd.read_csv(fname)
    df = df[df["param"].isin(params)]
    df = df.rename(columns={"ocns.score": "score",
                            "value": "mean",
                            "ub": "upper", "lb": "lower"})
    df["method"] = method
    dfs.append(df)
lm = pd.concat(dfs).reset_index(drop=True)
lm["score"] = lm["score"].str.replace("c", "").astype(int)

dg = lm.groupby(["method", "param"])

fig, axs = plt.subplots(ncols=2, nrows=2, sharex=True, sharey=True,
                        figsize=(mplutil.twocol_width * 0.68, 3.2))

for midx, method in enumerate(methods):
    for pidx, param in enumerate(params):
        ax = axs[pidx, midx]
        mlm = lm.loc[dg.groups[(method, param)]]
        pointrange(mlm["score"].values,
                   mlm["mean"].values, mlm["lower"].values, mlm["upper"].values,
                   markersize=4, mec="none", color=colors.base,
                   zorder=2, ax=ax)
        ax.axhline(0, ls="--", color=colors.light_gray, zorder=1)
        mplutil.remove_spines(ax)


for ax, el in zip(axs[:, -1], ["OCNS", "HACNS"]):
    ax.text(1.1, 0.55,
            el.upper(),
            transform=ax.transAxes, rotation=-90,
            ha="center", va="center", fontsize="medium")

axs[0, 0].set_xlim(lm["score"].min() - 15, lm["score"].max() + 15)

axs[0, 0].locator_params(axis="x", nbins=5)

fig.subplots_adjust(left=0.115, right=0.94, bottom=0.12, top=0.95,
                    wspace=0.1, hspace=0.15)

labels = ["No target size adjustment", "Target size adjustment"]
for ax, label in zip(axs[0, :], labels):
    ax.text(0.5, 1.05, label,
            transform=ax.transAxes, ha="center", fontsize="medium")

mplutil.shared_ylabel(axs[:, 0], "Coefficient value")
mplutil.shared_xlabel(axs[-1, :], "phastCons score threshold")

fig.savefig("lm-score-ragg-coefs.pdf")
