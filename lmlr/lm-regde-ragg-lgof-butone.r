#!/usr/bin/env Rscript

library(reshape)
library(parallel)

de <- as.matrix(read.table("../limma-de/region-de-butone.dat", sep=" "))
colnames(de) <- NULL

de.names <- read.table("../limma-de/region-de-butone-names.txt",
                       sep=" ", header=TRUE)
name.vec <- do.call(function(...) paste(..., sep="."), de.names)

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv")
ng <- ng[c("ocns_nolgof", "lof", "gof")]
ng <- log1p(ng)

rand <- read.csv(
    "../data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv")
rand <- rand[,colnames(rand) != "ensg"]
rand <- log1p(rand)
rand.agg <- apply(rand, 1, median)

run_logistic <- function(vals){
    dat <- cbind(de=vals, ng)
    glm(de ~ ocns_nolgof + lof + gof + rand.agg,
        data=dat, family=binomial(link="probit"))
}

fits <- apply(de, 2, run_logistic)

coefs <- sapply(fits, coef)
rownames(coefs)[1]  <- "intercept"
colnames(coefs) <- name.vec
scs <- melt(coefs, varnames=c("param", "drop.period"))
scs$param <- factor(scs$param, levels=unique(scs$param))
scs$drop.period <- factor(scs$drop.period)

bounds <- mclapply(fits, function (x) confint(x),
                   mc.cores=4)

create.bounds.df <- function (idx){
    d <- data.frame(drop.period=name.vec[idx],
                    lb=bounds[[idx]][,1], ub=bounds[[idx]][,2])
    rownames(d)[1] <- "intercept"
    d$param <- rownames(d)
    rownames(d) <- NULL
    return(d)
}

sbs <- do.call(rbind, lapply(1:length(bounds), create.bounds.df))
scs <- merge(sbs, scs)

write.csv(scs, file="lm-regde-ragg-lgof-butone.csv", row.names=FALSE, quote=FALSE)
