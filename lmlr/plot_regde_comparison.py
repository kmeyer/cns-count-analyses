#!/usr/bin/env python3

import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import numpy as np
import pandas as pd

import colors
import mcmcstats
import mplutil
import periodax
from plots import pointrange
import stanio

mplutil.style_use(["latex"])

params = ["ocns", "hacns"]
periods = np.arange(1, 16)

## glm

lm = pd.read_csv("lm-regde-ragg.csv")
lm = lm[lm["param"].isin(params)]

lm = lm.rename(columns={"value": "mean",
                        "ub": "p975", "lb": "p025"})
lm["method"] = "lm"

## dp

dp_fname = "../count-regr-lde/samples/dp__regde_ragg-all-chains-all-periods.j4.csv"
dp_sim = stanio.read_samples(dp_fname, filter_svars=True)

dp_cols = ["p{}_beta__{}".format(p, el) for p in periods for el in [2, 3]]
dp_sim = dp_sim[dp_cols]

dp = mcmcstats.interval_summary(dp_sim)

dp["period"], _, _, dp["param"] = zip(*dp.index.str.split("_"))
dp["param"] = dp["param"].map({"2": "ocns", "3": "hacns"})
dp["period"] = dp["period"].str.replace("p", "").astype(int)
dp["method"] = "dp"

##

d = pd.concat([lm, dp])
d = d.reset_index(drop=True)
d = d.sort_values("period")

dg = d.groupby(["method", "param"])

fig, axs = plt.subplots(ncols=1, nrows=2, sharex=True,
                        figsize=(mplutil.twocol_width * 0.63, 3.5))

meth_offset = {"lm": -0.25, "dp": 0.2}
meth_colors = {"lm": colors.base,
               "dp": colors.primary,}

for ax, param in zip(axs, params):
    for meth in ["lm", "dp"]:
        ad = d.loc[dg.groups[(meth, param)]]
        mcol = meth_colors[meth]
        pointrange(ad["period"].values + meth_offset[meth],
                   ad["mean"].values, ad["p025"].values, ad["p975"].values,
                   markersize=5, mec="none", color=mcol,
                   zorder=2, ax=ax)

    ax.set_xticks(ad["period"].values)
    ax.set_xlim(periods[0] - 0.5, periods[-1] + 0.6)

    mplutil.remove_spines(ax)
    ax.tick_params(axis="x", length=0)

    periodax.period_vlines(ax, start=1)

    ax.locator_params(axis="y", nbins=6)

axs[-1].axhline(0, ls="--", color=colors.light_gray, zorder=1)

axs[0].text(2.55, 0.40, "ML", fontsize="small",
            color=meth_colors["lm"])
axs[0].text(2.55, 0.35, "MCMC", fontsize="small",
            color=meth_colors["dp"])

formatter = FuncFormatter(lambda x, _: "${:.2f}$".format(x))
axs[1].yaxis.set_major_formatter(formatter)

axs[1].locator_params(axis="y", nbins=6)

fig.subplots_adjust(left=0.14, right=0.99, bottom=0.145, top=0.97,
                    wspace=0.2, hspace=0.2)

for ax, el in zip(axs, ["OCNS", "HACNS"]):
    ax.text(0.5, 1.01, el, ha="center", fontsize="small",
            transform=ax.transAxes)

mplutil.shared_ylabel(axs, "Coefficient value")
periodax.label_groups(axs[1], start=1)

fig.savefig("dp-lm-coef-comparison.pdf")
