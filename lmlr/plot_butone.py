#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mcmcstats
import mplutil
import periodax
from plots import pointrange

mplutil.style_use(["latex"])

df = pd.read_csv("lm-regde-ragg-butone.csv")
df["dropped_region"], df["period"] = zip(*df["drop.period"].str.split("."))
df["period"] = df["period"].astype(np.int)
df = df.drop("drop.period", axis=1)

df_ocns = df[df["param"] == "ocns"]

df_all = pd.read_csv("lm-regde-ragg.csv")
df_all = df_all[df_all["period"] > 2]
df_all = df_all[df_all["param"] == "ocns"]

fig, axs = plt.subplots(nrows=3, ncols=2,
                        sharey=True, sharex=True,
                        figsize=(mplutil.twocol_width * 0.75, 4))

regions = ["ncx", "hip", "amy", "str", "md", "cbc"]
regions_full = {s: f
                for s, f in zip(regions,
                                ["neocortex", "hippocampus", "amygdala",
                                 "striatum", "thalamus", "cerebellum"])}

for ax, reg in zip(axs.ravel(), regions):
    df_reg = df_ocns[df_ocns["dropped_region"] == reg]

    pointrange(df_reg["period"].values + 0.2,
               df_reg["value"].values,
               df_reg["lb"].values, df_reg["ub"].values,
               markersize=4, mec="none", color=colors.primary,
               zorder=2, ax=ax)

    pointrange(df_all["period"].values - 0.2,
               df_all["value"].values,
               df_all["lb"].values, df_all["ub"].values,
               markersize=4, mec="none", color=colors.base,
               zorder=2, ax=ax)

    ax.text(0.5, 0.99, "without {}".format(regions_full[reg]),
            ha="center", va="center",
            fontsize="small", color=colors.primary,
            transform=ax.transAxes)

    mplutil.remove_spines(ax)
    periodax.period_vlines(ax, start=3)

# axs[0, 0].set_ylim(0)

axs[0, 0].locator_params(nbins=4)

ax.set_xticks(np.arange(3, 16))

fig.subplots_adjust(left=0.085, right=0.99, bottom=0.12, top=0.98,
                    wspace=0.08, hspace=0.15)

mplutil.shared_ylabel(axs[:, 0], "OCNS coefficient")

for ax in axs[-1, :]:
    periodax.label_groups(ax, start=3)

axs[0, 0].set_xlim(2.5, 15.5)

fig.savefig("ocns-coef-butone.pdf")
