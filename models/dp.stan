
data {
  int<lower=1> N;
  int<lower=1> K;

  matrix[N,K] x;

  int<lower=0,upper=1> y[N];
}

parameters {
  vector[K] beta;
}

model {
  to_vector(beta) ~ normal(0, 3);
  for (n in 1:N)
    y[n] ~ bernoulli(Phi_approx(x[n] * beta));
}
