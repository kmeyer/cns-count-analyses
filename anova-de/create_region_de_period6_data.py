#!/usr/bin/env python3

import numpy as np
import pandas as pd
import sys

import rdump

de = pd.read_table("region-de.dat", header=None, sep=" ")

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv",
                 usecols=["ocns", "hacns", "cacns", "macns"])
ng = np.log1p(ng)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values])

data = {}
data["y"] = np.squeeze(de.values)
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data, "dp__region_de_period6.data.r")
