#!/usr/bin/env Rscript

library(limma)

load("region-de.rdata")

cm <- makeContrasts(contrasts="region1-region2",
                    levels=design)

c.fit <- treat(contrasts.fit(fit, contrasts=cm), lfc=1)


de <- decideTests(c.fit, method="global", adjust.method="BH",
                  p.value=0.01)

write.csv(de, file="region-de.csv", row.names=FALSE, quote=FALSE)
