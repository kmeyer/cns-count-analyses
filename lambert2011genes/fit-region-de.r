#!/usr/bin/env Rscript

library(limma)
library(rhdf5)

datafile <- "../data/expression/lambert2011genes.hdf5"
## genes x samples
expr <- t(as.matrix(h5read(datafile, "/values")))

## 32-bit integer warnings
suppressWarnings(di <- h5read(datafile, "/di") + 1)
suppressWarnings(dr <- h5read(datafile, "/dr") + 1)

dr <- factor(dr)
di <- factor(di)

x <- data.frame(region=dr, indiv=di)
design <- model.matrix(~ 0 + region, data=x)

dupcor <- duplicateCorrelation(expr,
                               design=design, ndups=1, block=x$indiv)
fit <- lmFit(expr, design, block=x$indiv, correlation=dupcor$consensus)

save(x, design, dupcor, fit, file="region-de.rdata")
