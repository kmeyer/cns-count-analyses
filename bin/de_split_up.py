#!/usr/bin/env python3
"""Split pairwise DE output to upregulated column for each factor.

Usage: ./de_split_up.py <de-file> <outfile>
"""
import sys

import numpy as np
import pandas as pd

try:
    _, defile, outfile = sys.argv
except ValueError:
    sys.exit(__doc__)

de = pd.read_csv(defile)

de0 = (de > 0).astype(np.int)
de1 = (de < 0).astype(np.int)

def reverse_contrast(label):
    return " ".join(list(reversed(label.partition("-")))).strip()

de1.rename(columns=reverse_contrast,
           inplace=True)

de_full = pd.concat([de0, de1], axis=1)

de_full.to_csv(outfile, index=False)
