
BIN = $(HOME)/.local/bin
PYLIB = $(HOME)/.local/lib/python

stowdir = ./.stow
name = src
pkg = $(stowdir)/$(name)

.PHONY: build
build:
	rm -rf $(stowdir)
	mkdir -p $(pkg)/$(BIN)
	install -m744 src/bin/* $(pkg)/$(BIN)
	mkdir -p $(pkg)/$(PYLIB)
	install -m644 src/lib/python/*.py $(pkg)/$(PYLIB)

.PHONY: install
install:
	stow -v -d $(stowdir) -t / $(name)

.PHONY: uninstall
uninstall:
	stow -v -d $(stowdir) -t / -D $(name)
