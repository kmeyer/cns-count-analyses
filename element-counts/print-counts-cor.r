#!/usr/bin/env Rscript

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")

cat("ocns vs hacns, nearest at least one hacns\n")
print(cor(ng$ocns[ng$hacns > 0], ng$hacns[ng$hacns > 0], method="spearman"))

cat("ocns vs cacns, nearest at least one cacns\n")
print(cor(ng$ocns[ng$cacns > 0], ng$cacns[ng$cacns > 0], method="spearman"))

cat("ocns vs macns, nearest at least one macns\n")
print(cor(ng$ocns[ng$macns > 0], ng$macns[ng$macns > 0], method="spearman"))
