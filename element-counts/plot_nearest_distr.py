#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import colors
import mplutil

mplutil.style_use(["latex"])

cols = ["ocns", "hacns", "cacns", "macns"]
ngc = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ngc = ngc[cols]
ngc["ocns"] = np.log1p(ngc["ocns"])

fig, axs = plt.subplots(ncols=2, figsize=(mplutil.twocol_width * 0.73, 2.1))

## OCNS

ax_ocns = axs[0]

ax_ocns.hist(ngc.loc[ngc["ocns"] > 0, "ocns"].values,
             bins=20, ec=colors.base,
             cumulative=True, histtype="step")

ax_ocns.set_ylabel("Cumulative number of genes")
ax_ocns.set_xlabel(r"$\ln(1 + \text{Number of OCNSs})$")
mplutil.remove_spines(ax_ocns)

ax_ocns.set_xlim(np.log1p(1) * 0.9, ngc["ocns"].max() * 1.05)
ax_ocns.locator_params(axis="y", nbins=6)

## ACNS

ax_acns = axs[1]

el_colors = [colors.primary, colors.secondary, colors.base]

for idx, el in enumerate(ngc.columns[1:]):
    vals = ngc[ngc[el] > 0][el].values
    ## Make single-value width bins, accounting for the last bin being
    ## [x_{N-1}, x_N].
    bins = np.arange(1, vals.max() + 2)

    if el == "hacns":
        yr = 0.6
        xr = 1.2
        zorder = 3
    elif el == "macns":
        yr = 1.03
        xr = 0.7
        zorder = 1
    else:
        yr = 1.05
        xr = 0.7
        zorder = 2

    heights, _, _ = ax_acns.hist(ngc[ngc[el] > 0][el].values, bins=bins,
                                 ec=el_colors[idx], zorder=zorder,
                                 cumulative=True, histtype="step")

    ax_acns.text(vals.max() * xr, heights.max() * yr,
                 el.upper(),
                 color=el_colors[idx], fontsize="small")

ax_acns.set_ylabel("Cumulative number of genes")
ax_acns.set_xlabel("Number of elements")

ax_acns.set_xlim(0.5)
ax_acns.locator_params(axis="y", nbins=5)

mplutil.remove_spines(ax_acns)

fig.subplots_adjust(left=0.13, right=0.97, bottom=0.17, top=0.92,
                    wspace=0.4, hspace=0.2)

mplutil.subplot_label(ax_ocns, r"\textbf{A}")
mplutil.subplot_label(ax_acns, r"\textbf{B}")

fig.savefig("nearest-distr.pdf")
