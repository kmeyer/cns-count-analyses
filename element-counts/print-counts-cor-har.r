#!/usr/bin/env Rscript

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts-har.csv")

cat("ocns vs har, nearest at least one har\n")
print(cor(ng$ocns_nohp[ng$har > 0], ng$har[ng$har > 0], method="spearman"))
