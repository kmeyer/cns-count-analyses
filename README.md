
This repository contains the analyses for the manuscript "Differential
gene expression in the human brain is associated with conserved, but
not accelerated, noncoding sequences".

http://dx.doi.org/10.1093/molbev/msx076

------------------------------------------------------------------------

Dependencies
============

These analyses have been developed in a GNU/Linux environment.  In
addition to standard GNU/Linux utilities, this repository has several
dependencies.  Unless stated otherwise, the versions listed are the
versions used, but earlier versions may work.

Snakemake
---------

Snakemake (v3.5.5) is required to run the build rules that are
provided for all analyses.

<https://bitbucket.org/snakemake/snakemake/wiki/Home>

Python
------

Python 3.3 or greater is required.

Third-party packages:

-   docopt (v0.6.2)
-   h5py (v2.5.0)
-   matplotlib (v1.5.1)
-   numpy (v1.10.4)
-   pandas (v0.18.0)
-   pytest (v2.9.1) (optional)
-   scipy (v0.17.0)
-   xlrd (v0.9.4)

R packages
----------

In addition to base R (v3.2.3), the following packages are used.

-   ggplot2 (v1.0) (optional)
-   limma (v3.26.7)
-   mgcv (v1.8-15)
-   rhdf5 (v2.14.0)
-   statmod (v1.4.24)
-   topGO (v2.2.0)

Other dependencies
------------------

-   bedtools (v2.24.0)

-   bigWigAverageOverBed (MD5: e4c0c0b9013141d48c084007e169d494)

    <http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bigWigAverageOverBed>

-   CmdStan (v2.6.2)

    <http://mc-stan.org/interfaces/cmdstan.html>

-   GNU Stow (v.2.2.0) (optional)

    This is used to put executables and Python modules in the right
    location.  See the second step of "Setup" for more information.

-   LaTeX

-   liftOver (MD5: 8a09591668409c774432456ea18d3515)

    <http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/liftOver>


Required setup
==============

1.  Specify the location of CmdStan.

    Change the value of `cmdstan_dir` in sm/config.json to point to
    the location of the CmdStan repository.

2.  Put src/bin and src/lib files in appropriate search paths.

    The files under /path/to/here/src/bin/ are called without
    specifying a path, so they should be placed within a directory
    that's included in `$PATH` (or `$PATH` should be extended to
    include the src/bin/ directory).

    The python files in /path/to/here/src/lib/python/ are imported as
    modules.  They should be copied to a directory in `$PYTHONPATH`
    (or `$PYTHONPATH` should be extended to include src/lib/python/).

    Alternatively, if you have Stow installed, you can configure `BIN`
    (default: $HOME/.local/bin) and `PYLIB` (default:
    $HOME/.local/lib/python) in the Makefile.  `BIN` should be in
    `$PATH`, and `PYLIB` should be in `$PYTHONPATH`.

    Then running `make && make install` will create links the
    appropriate locations.  The main advantage of using Stow is that
    the links can be uninstalled in one command (`make uninstall`).

3.  Place src/latex.mplstyle in <mpl-config>/matplotlib/stylelib,
    where mpl-config is typically ~/.config/matplotlib.  (You can use
    `matplotlib.get_configdir()` to check.)

    This is required because many of the figures rely on LaTeX for
    text rendering and will fail without the LaTeX setup specified in
    this file.

    See http://matplotlib.org/users/style_sheets.html for more
    information on Matplotlib style sheets.

Running analyses
================

All analyses should be executed through Snakemake, which will take
care of building the dependencies.

Running `snakemake` with no arguments in the top-level directory will
print the target names for all the figures as well as "stats", which
refers to any specific numbers included in the text or tables.

To see which rules would be executed in order to build a target, you
can pass the `--dryrun` flag.  For example,

    $ snakemake --dryrun element-counts/ocns-hist-hc-vs-ocns.pdf

will report everything that needs to be done to generate the figure
element-counts/ocns-hist-hc-vs-ocns.pdf.  To actually execute these
steps, remove the `--dryrun` flag.

See `snakemake --help` for more details on running `snakemake`.

There are a few paths that you likely want avoid running locally
because they take a long time to complete.  Below are rules that take
over an hour to run, along with their very approximate expected
execution times.

-   limma_de_fit_pair_de_ (within 1 day)
-   gtex_fit_de_ (within 3 days)

Data set downloads
------------------

In general, data sets will be downloaded automatically if they are not
already present.  GTEx data files are exceptions because an account is
required to download them.  To run analyses that depend on GTEx data,
manually download the three files below to the directory
expression/gtex/.

-   GTEx_Analysis_v6_RNA-seq_RNA-SeQCv1.1.8_gene_reads.gct.gz
-   GTEx_Data_V6_Annotations_SampleAttributesDS.txt
-   GTEx_Data_V6_Annotations_SubjectPhenotypesDS.txt

<http://www.gtexportal.org/home/datasets>

You may also want to manually download the files
data/expression/GPL5175-3188.txt and data/expression/GPL570-13270.txt
because, unlike the other NCBI downloads, these go through http rather
than ftp, so the server may flag and deny the download.  You can get
them through the "Download full table" button at the links below.

<http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL5175>
<http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL570>


License
=======

All code in this repository is released under the GNU General Public
License, version 3.
