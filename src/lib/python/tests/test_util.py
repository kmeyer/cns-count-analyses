import pytest
import util
import re


def test_patten_matches_default():
    pats = [re.compile(p) for p in [r'[a-z]+', r'[0-9]+']]
    strings = ['worda 1234', '5678 wordb']
    result = list(util.pattern_matches(pats, strings))

    assert result == [['worda', '1234'],
                      ['wordb', '5678']]


def test_patten_matches_groups():
    pats = [re.compile(p)
            for p in [r'[a-z]+([a-z])', r'([0-9])([0-9])[0-9]+']]
    strings = ['worda 1234', '5678 wordb']
    result = list(util.pattern_matches(pats, strings, groups=[1, 2]))

    assert result == [['a', '2'],
                      ['b', '6']]


def test_patten_matches_group_pattern_mismatch():
    with pytest.raises(ValueError):
        list(util.pattern_matches(['_', '_'], ['_', '_'], groups=[1]))
