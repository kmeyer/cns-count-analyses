import numpy as np
import pandas as pd
import pytest

import pdutil


def test_filter_repeats():
    df = pd.DataFrame({"id": ["a", "b", "b", "c", "c"],
                       "va": [1, 2, 3, 1, 1],
                       "vb": [1, 6, 5, 2, 4]})

    result = pdutil.filter_repeats(df, "id", ["va", "vb"])

    assert result.shape[0] == 3
    assert np.all(result.loc[result["id"] == "b", "va"] == [3])
    assert np.all(result.loc[result["id"] == "c", "vb"] == [4])


def test_filter_repeats_nonunique():
    df = pd.DataFrame({"id": ["a", "b", "b", "c", "c"],
                       "va": [1, 2, 3, 1, 1],
                       "vb": [1, 6, 5, 2, 2]})

    with pytest.raises(ValueError):
        pdutil.filter_repeats(df, "id", ["va", "vb"])
