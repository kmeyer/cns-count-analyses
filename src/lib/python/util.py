import os.path
import subprocess


def pattern_matches(patterns, strings, groups=None):
    """Extract `patterns` from each item in `strings`.

    Parameters
    ----------
    patterns : list of pattern objects
        Patterns generated with `re.compile`.
    strings : list of strings
    groups : list of integers
        A group for each item in `patterns` that should be returned.
        If None, 0 is used for all patterns.

    Returns
    -------
    Generator that yields a list of matches for each item in
    `strings`.
    """
    if groups is None:
        groups = [0] * len(patterns)
    else:
        if len(groups) != len(patterns):
            raise ValueError('Number of patterns must equal number of groups')

    for s in strings:
        yield [p.search(s).group(g) for p, g in zip(patterns, groups)]
