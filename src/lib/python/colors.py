lighter_gray = '0.9'
light_gray = '0.7'
gray = '0.5'
dark_gray = '0.4'
darker_gray = '0.1'

base = "0.2"
primary = "#2166ac"
secondary = "#8c510a"
face = lighter_gray
cmap = "Greens"
cmap_div = "PRGn"
