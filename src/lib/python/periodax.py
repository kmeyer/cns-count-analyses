from matplotlib import transforms
from matplotlib import lines
from matplotlib.tight_layout import get_renderer

import colors


def label_groups(ax, start=3, labelpad=None, line_kws=None, text_kws=None):
    """Add time group labels to the x-axis of `ax`.

    Parameters
    ----------
    ax : AxesSubplot instance
    start : int
      First period
    labelpad : float or None
    line_kws : dict or None
        Keyword arguments passed to `matplotlib.lines.Lines2D`
    text_kws : dict or None
        Keyword arguments passed to `ax.text`
    """
    fig = ax.figure

    line_tbox_raw = ax.get_tightbbox(get_renderer(fig))
    text_tbox_raw = transforms.Bbox(line_tbox_raw.get_points().copy())
    label_tbox_raw = transforms.Bbox(line_tbox_raw.get_points().copy())

    if labelpad is None:
        labelpad = ax.xaxis.labelpad
    offset = labelpad * fig.dpi / 72.0

    line_tbox_raw.y0 -= offset * 0.3
    text_tbox_raw.y0 -= offset * 1.85
    label_tbox_raw.y0 -= offset * 4

    line_tbox = transforms.TransformedBbox(line_tbox_raw,
                                           fig.transFigure.inverted())
    text_tbox = transforms.TransformedBbox(text_tbox_raw,
                                           fig.transFigure.inverted())
    label_tbox = transforms.TransformedBbox(label_tbox_raw,
                                            fig.transFigure.inverted())

    trans = transforms.blended_transform_factory(ax.transData,
                                                 fig.transFigure)

    groups = ["Prenatal", "Early postnatal", "Adult"]
    periods = [list(range(start, 8)),
               list(range(8, 13)),
               list(range(13, 16))]
    xs = [(p[0] - 0.25, p[-1] + 0.25) for p in periods]

    line_opts = {"lw": 0.5, "color": colors.base}
    if line_kws is not None:
        line_opts.update(line_kws)

    line_y = line_tbox.y0
    for x in xs:
        line = lines.Line2D(x, [line_y, line_y], transform=trans,
                            **line_opts)
        line.set_clip_on(False)
        ax.add_line(line)

    text_opts = {"ha": "center",
                 "fontsize": "small"}
    if text_kws is not None:
        text_opts.update(text_kws)

    mids = [start + (7 - start) // 2,
            10, 14]
    for group, mid in zip(groups, mids):
        ax.text(mid, text_tbox.y0, group, transform=trans,
                **text_opts)

    ax.text(start + (15 - start) / 2,
            label_tbox.y0, "Time period", transform=trans,
            ha="center")


def period_vlines(ax, start=3):
    """Draw vertical lines separated time periods.

    Parameters
    ----------
    ax : AxesSubplot instance
    start : int
      First period
    """
    for per in range(start, 16):
        if per == 7:
            ax.axvline(per + 0.5, color="0.5", lw=0.5, alpha=0.6)
        else:
            ax.axvline(per + 0.5, color="0.8", lw=0.5, alpha=0.6)
