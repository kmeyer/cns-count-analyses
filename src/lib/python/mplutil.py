
import matplotlib as mpl
from matplotlib import ticker
from matplotlib.tight_layout import get_renderer
from matplotlib.transforms import TransformedBbox, Bbox
from warnings import warn

import colors

ltypes = ['-', '--', ':', '-.']
mtypes = ['.', 'o', 'v', '^', '<', '>', '1', '2', '3', '4', 's',
          'p', '*', 'h', 'H', '+', 'x', 'D', 'd', '|', '_',]

gray_bars = {'edgecolor': colors.darker_gray, 'color': colors.lighter_gray}

onecol_width, twocol_width = 3.25, 6.75


def style_use(style):
    """Safe version of `matplotlib.style.use`

    If `style` is not found, warn instead of raising error.
    """
    try:
        from matplotlib.style import use
        use(style)
    except OSError:
        warn('Style "{}" not found'.format(style))


def remove_spines(ax, which=None):
    """Remove spines from ax.

    `which` is a list that can contain 'top', 'left', 'right', or
    'bottom'. If `which` is None, defaults to 'top' and 'right'.
    """
    if which is None:
        which = ['top', 'right']
    for loc in which:
        ax.spines[loc].set_visible(False)
    ax.tick_params('both', **{loc: 'off' for loc in which})


def get_figax(ax, **kwds):
    if ax is None:
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(**kwds)
    else:
        fig = ax.figure
    return fig, ax


def shared_ylabel(axs, label, labelpad=None, **kwds):
    """Create shared y-axis label.

    Parameters
    ----------
    axs : list
        Axes to share the label
    label : string
    labelpad : float or None

    All other keyword arguments are passed as text parameters.

    Returns
    -------
    matplotlib.text.Text instance
    """
    return _shared_label('y', axs, label, labelpad, **kwds)


def shared_xlabel(axs, label, labelpad=None, **kwds):
    """Create shared x-axis label.

    Parameters
    ----------
    axs : list
        Axes to share the label
    label : string
    labelpad : float or None

    All other keyword arguments are passed as text parameters.

    Returns
    -------
    matplotlib.text.Text instance
    """
    return _shared_label('x', axs, label, labelpad, **kwds)


def _shared_label(axis, axs, label, labelpad=None, **kwds):
    fig = axs[0].figure

    ## Use the combined tight bbox to adjust for the labels.
    tbox_raw = Bbox.union([ax.get_tightbbox(get_renderer(fig))
                           for ax in axs])

    naxs = len(axs)
    if naxs < 2:
        raise ValueError('This function only makes sense'
                         'with multiple axes')
    if naxs % 2 != 0:  # Use single center axis.
        lo_indx = hi_idx = naxs // 2
    else:
        hi_idx = naxs // 2
        lo_indx = hi_idx - 1

    if axis == 'y':
        axis, to_adjust = 'yaxis', 'x0'
        text_opts = {'va': 'center', 'ha': 'right', 'rotation': 90}

        ## The combined bbox isn't used here (or below) so that,
        ## regardless of the tick labels, the axis label is in the
        ## center of the middle axis (if odd number of axes) or in
        ## between the middle two (if even number of axes).
        ylower, yupper = axs[lo_indx].figbox.y0, axs[hi_idx].figbox.y1
        ypos = (ylower + yupper) / 2

        def coordf(b):
            return [b.x0, ypos]
    elif axis == 'x':
        axis, to_adjust = 'xaxis', 'y0'
        text_opts = {'va': 'top', 'ha': 'center'}

        xlower, xupper = axs[lo_indx].figbox.x0, axs[hi_idx].figbox.x1
        xpos = (xlower + xupper) / 2

        def coordf(b):
            return [xpos, b.y0]
    else:
        raise ValueError('Axis must be x or y')

    text_opts.update(kwds)

    if labelpad is None:
        labelpad = getattr(axs[0], axis).labelpad
    offset = labelpad * fig.dpi / 72.0
    setattr(tbox_raw, to_adjust, getattr(tbox_raw, to_adjust) - offset)

    tbox = TransformedBbox(tbox_raw, fig.transFigure.inverted())
    xpos, ypos = coordf(tbox)
    return fig.text(xpos, ypos, label, **text_opts)


def subplot_label(ax, label, xpos=None, ypos=None, xpad=3, ypad=6, **kwds):
    """Add label to subplot.

    Parameters
    ----------
    ax : matplotlib Axes instance
    label : str
        Text to add
    xpos, ypos : float or None
       Figure position.  If not None, this position is used instead of
       the position calculated with `xpad` or `ypad`.
    xpad, ypad : float
        If `xpad` is None, the x-position is determined by subtracting
        the value of `xpad` from the left x-boundary.  If `ypad` is
        None, the y-position is determined by adding the value of
        `ypad` the top y-boundary.

    Other keyword arguments are passed as options to the `Figure.text`
    method.

    Returns
    -------
    Tuple with the text position
    """
    fig = ax.figure
    box = ax.get_tightbbox(get_renderer(fig))

    x_offset = xpad * fig.dpi / 72.0
    box.x0 = box.x0 - x_offset
    y_offset = ypad * fig.dpi / 72.0
    box.y1 = box.y1 + y_offset
    tbox = TransformedBbox(box, fig.transFigure.inverted())

    if xpos is None:
        xpos = tbox.x0
    if ypos is None:
        ypos = tbox.y1

    text_opts = {"fontsize": "large",
                 "va": "top"}
    text_opts.update(**kwds)

    fig.text(xpos, ypos, label, **text_opts)
    return xpos, ypos


def style_heatmap(ax, axis_offset=0.15):
    old_xlim = ax.get_xlim()
    ax.set_xlim(old_xlim[0] - axis_offset, old_xlim[1] + axis_offset)

    old_ylim = ax.get_ylim()
    ax.set_ylim(old_ylim[0] + axis_offset, old_ylim[1] - axis_offset)

    for pos in ax.get_yticks():
        ax.axhline(pos + 0.5, color="w", zorder=2)
    for pos in ax.get_xticks():
        ax.axvline(pos + 0.5, color="w", zorder=2)
    ax.plot([0, 1], [1, 0], transform=ax.transAxes, zorder=3,
            color=colors.base, lw=0.75 * mpl.rcParams["lines.linewidth"],
            alpha=0.7)


def heat_colorbar(cax, im, label, nbins=6, tick_left=True, **kwds):
    label_opts = {"va": "bottom", "size": "small", "rotation": -90}
    label_opts.update(kwds)

    cbar = cax.figure.colorbar(im, cax=cax)
    cbar.ax.tick_params(labelsize=label_opts["size"])
    if tick_left:
        cbar.ax.yaxis.set_ticks_position("left")
    cbar.locator = ticker.MaxNLocator(nbins=nbins)
    cbar.update_ticks()
    cbar.set_label(label, **label_opts)

    return cbar
