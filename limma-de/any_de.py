#!/usr/bin/env python3
"""Collapse DE for contrasts.

Usage: any_de.py <de-file> <h5file> <outfile>

If the columns names are formed as "pr<N>.<X> - pr<N>.<Y>", collapse
per time period N rather than collapsing all columns.
"""

import re

from docopt import docopt
import numpy as np
import pandas as pd

args = docopt(__doc__)

de_dabg = pd.read_csv(args["<de-file>"])

with pd.get_store(args["<h5file>"], mode="r") as store:
    pass_dabg = store["pass_dabg"].values

de = np.zeros((len(pass_dabg), de_dabg.shape[1]), dtype=np.int)
de[pass_dabg] = de_dabg.values
de = pd.DataFrame(de, columns=de_dabg.columns)
de.index.name = "gene"

PR_RE = re.compile(r"pr([0-9]+)\.([0-9]+) - pr([0-9]+)\.([0-9])+$")
if PR_RE.match(de.columns[0]):

    def period_regions(label):
        m = PR_RE.match(label)
        period = m.group(1)
        if period != m.group(3):
            raise ValueError("Label {} has contrasts between different periods")
        return int(period), int(m.group(2)), int(m.group(4))

    de.columns = pd.MultiIndex.from_tuples(de.columns.map(period_regions),
                                           names=("period", "reg1", "reg2"))
    de = de.stack(de.columns.names)
    de.name = "de"
    de = de.reset_index()
    de["de_abs"] = de["de"].abs()

    de_any = (de.groupby(["gene", "period"])["de_abs"].sum() > 0)
    de_any = de_any.astype(np.int).unstack("period")

else:
    de_any = de.abs().any(1).astype(np.int)

de_any.to_csv(args["<outfile>"], sep=" ", index=False, header=False)
