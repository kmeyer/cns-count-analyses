#!/usr/bin/env python3

import sys
from pprint import pprint

import numpy as np

try:
    defile = sys.argv[1]
except (ValueError, IndexError):
    sys.exit("Usage: {} <de-file>".format(sys.argv[0]))

de = np.loadtxt(defile, dtype=np.int)

print(defile)
pprint(de.sum(0).tolist())
